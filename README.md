# Monte Carlo Tree Search (MCTS)
## _Jeu de Brix Version Débutant_

## Informations
- Importer les fichiers du joueur voulu
- Modifier les joueurs instanciés dans `arbitre.cc`
- Adapter le `main.cc` en fonction des joueurs mis en compétition et du nombre de parties par challenge (paramètres du constructeur d'`Arbitre`)
- Modifier la constante `TEMPS_POUR_UN_COUP` dans `arbitre.hh` (temps en milisecondes) pour adapter le temps laissé au joueur pour jouer
- Déposer dans le répertoire de build le fichier `ABBEL_save.MCTS` pour permettre la lecture du fichier contenant l'arbre

